Name: connectathon
Version: 1.19
Release: 1%{?dist}
Summary: The Connectathon Test Suite

Group: Utilities/Benchmarking
License: Oracle
URL: https://fedorapeople.org/cgit/steved/public_git/cthon04.git/
Source: cthon04.tar.gz
BuildRoot: /tmp/%{name}-buildroot

%description
The Connectathon NFS Test Suite (from Steve Dickson git repository)

%prep
%setup -q -n cthon04

%build
make

%install
rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}/usr/local/bin/%{name}
make copy DESTDIR=${RPM_BUILD_ROOT}/usr/local/bin/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/usr/local/bin/%{name}/*

%changelog
