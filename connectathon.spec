Name: connectathon
Version: 1.19
Release: 1%{?dist}
Summary: The Connectathon Test Suite

Group: Utilities/Benchmarking
License: Oracle
URL: https://fedorapeople.org/cgit/steved/public_git/cthon04.git/
Source: cthon04.tar.gz
Patch0: wc-custom.patch
BuildRoot: /tmp/%{name}-buildroot

%description
The Connectathon NFS Test Suite (from Steve Dickson git repository)

%prep
%setup -q -n cthon04
%patch0 -p1

%build
make

%install
rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}/opt/%{name}
make copy DESTDIR=${RPM_BUILD_ROOT}/opt/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/opt/%{name}/*

%changelog
* Thu Apr 22 2016 Saurabh Tandan <saurabh.tandan@intel.com>
- Updating Connectathon TestSuite
- Modified wc-custom.patch suitably for updated Connectathon TestSuite

* Wed Mar 24 2011 Michael MacDonald <mjmac@whamcloud.com>
- first packaging

