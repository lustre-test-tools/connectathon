TOPDIR=/tmp/rpmbuild
rm -rf $TOPDIR
rm -f *.rpm
mkdir -p $TOPDIR/SOURCES

tar czf $TOPDIR/SOURCES/cthon04.tar.gz cthon04
rpmbuild --define "_topdir $TOPDIR" -bs cthon04.spec

cp $TOPDIR/SRPMS/*.src.rpm .
rpmbuild --define "_topdir $TOPDIR" --define "_bindir /usr/local/bin/" --rebuild *.src.rpm
cp $TOPDIR/RPMS/*/* .